# Queues manager

**This is just an exercise program, nothing more!**

Queues managing program. For usage details please refer to the `cli_help`
function.

Doxygen docs are served using the gitlab pages:
[queue.h](https://mexus.gitlab.io/c-queues-exercise/queue_8h.html).

# Configuration

The program could be configured during the compilation with the following
options:

* `QUEUE_MAX_LENGTH`: a maximum length of the queues (defaults to 10). Please
                      don't set it to be more than UINT_MAX, otherwise the
                      program will misbehave.
* `QUEUE_MODE`: queues operation mode: **FIFO** (`QUEUE_MODE=QUEUE_MODE_FIFO`)
                or **LIFO** (`QUEUE_MODE=QUEUE_MODE_LIFO`). The default is LIFO.

# Building

```
$ cd build && cmake .. && make
```

To specify project options add them to the `cmake` command, like:

```
$ cd build && cmake .. -DQUEUE_MAX_LENGTH=1024 && make
```

# Testing the `queue` module.

To run the tests, first build the project, then run `make test` inside the
`build` directory:

```
$ cd build && cmake .. && make && make test
```

Please note that configuration options will have no effect on the tests.

On successful execution the return code will be zero; some output is
expected, but likely to be supressed by the `ctest` tool.

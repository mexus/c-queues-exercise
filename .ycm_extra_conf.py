import sys, os
import ycmflags

def FlagsForFile( filename ):
    base_path = os.path.dirname(os.path.abspath(__file__))
    flags = ycmflags.YcmFlags(default_file = ["queue.c", []],
            absolute_project_path = base_path)
    return flags.flags_for_file(filename)
